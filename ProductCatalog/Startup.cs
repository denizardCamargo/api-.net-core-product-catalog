﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Data;
using ProductCatalog.Repositories;
using Swashbuckle.AspNetCore.Swagger;

namespace ProductCatalog
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) //aqui se agregan los middlewares del pipeline(request/middlewares.../response), siempre se executa esto
        {
            services.AddMvc();

            services.AddResponseCompression(); //deixa todos os requests comprimidos(zip), que são descompactados no client

            services.AddScoped<StoreDataContext, StoreDataContext>(); //AddScoped: siempre que me pidan StoreDataContext, le doy StoreDataContext si no hay uno existente
            services.AddTransient<ProductRepository, ProductRepository>(); //AddTransient: siempre que me pidan ProductRepository, le doy un nuevo ProductRepository

            //services.AddSwaggerGen(x =>     //ele olha os endpoints da api e gera o documento, retorna uma url da aplicação
            //{
            //    x.SwaggerDoc("v1", new Info { Title = "Balta Store", Version = "v1" });
            //});
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseMvc();
            app.UseResponseCompression();

            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swager.json", "Balta Store - V1") //gera json creo
            //});


            //app.Run(async (context) => // Dice que solo escribe hello world y lo quitó
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //});
        }
    }
}
