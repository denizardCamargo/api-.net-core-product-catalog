﻿using Microsoft.EntityFrameworkCore;
using ProductCatalog.Data;
using ProductCatalog.Models;
using ProductCatalog.ViewModels.ProductViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Repositories
{
    public class ProductRepository // caso eu precise pegar dados de um lugar que não seja o sql server, eu configuro aqui no repository
    {
        private readonly StoreDataContext _context;

        public ProductRepository(StoreDataContext repository)
        {
            _context = repository;
        }

        public IEnumerable<ListProductViewModel> Get()
        {
            return _context
                //.Database.ExecuteSqlCommand //executar comando sql que seja um insert, delete ou update
                .Products
                .Include(x => x.Category)
                .Select(x => new ListProductViewModel
                {
                    Id = x.Id,
                    Title = x.Title,
                    Price = x.Price,
                    Category = x.Category.Title,
                    CategoryId = x.CategoryId
                })
                .AsNoTracking()
                .ToList();
        }
        public Product Get(int id)
        {
            return _context.Products.Find(id);
        }
        public void Save(Product product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();
        }
        public void Update(Product product)
        {
            _context.Entry<Product>(product).State = EntityState.Modified;
            _context.SaveChanges();
        }

    }
}
